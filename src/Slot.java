import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class Slot {

    private Opcoes opcao;

    public Opcoes getOpcao() {
        return opcao;
    }

    public Slot() {
        Random random = new Random();
        int tamanhoOpcoes = Opcoes.values().length;
        int valorAleatorio = random.nextInt(tamanhoOpcoes);

        this.opcao = Opcoes.values()[valorAleatorio];
    }

    @Override
    public String toString() {
        return "Slot: "
                 + opcao +
                " pontos: "+opcao.getPontos()+ " ";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Slot slot = (Slot) o;
        return opcao == slot.opcao;
    }

    @Override
    public int hashCode() {
        return Objects.hash(opcao);
    }
}
